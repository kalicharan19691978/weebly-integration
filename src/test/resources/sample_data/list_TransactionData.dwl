[
  {
    originator: "????" as String {class: "java.lang.String"},
    source_id: "????" as String {class: "java.lang.String"},
    actor_amount: 2 as Number {class: "java.lang.Long"},
    transaction_reference: "????" as String {class: "java.lang.String"},
    created_at: 2 as Number {class: "java.lang.Long"},
    recipient_amount: 2 as Number {class: "java.lang.Long"},
    transaction_type: "????" as String {class: "java.lang.String"},
    actor_currency_code: "????" as String {class: "java.lang.String"},
    actor_balance_token: "????" as String {class: "java.lang.String"},
    recipient_currency_code: "????" as String {class: "java.lang.String"}
  } as Object {class: "com.square.TransactionData"}
] as Array {mediaType: "*/*", encoding: "UTF-8", mimeType: "*/*", raw: [
  {
    originator: "????" as String {class: "java.lang.String"},
    source_id: "????" as String {class: "java.lang.String"},
    actor_amount: 2 as Number {class: "java.lang.Long"},
    transaction_reference: "????" as String {class: "java.lang.String"},
    created_at: 2 as Number {class: "java.lang.Long"},
    recipient_amount: 2 as Number {class: "java.lang.Long"},
    transaction_type: "????" as String {class: "java.lang.String"},
    actor_currency_code: "????" as String {class: "java.lang.String"},
    actor_balance_token: "????" as String {class: "java.lang.String"},
    recipient_currency_code: "????" as String {class: "java.lang.String"}
  } as Object {class: "com.square.TransactionData"}
] as Array {class: "java.util.ArrayList"}, class: "java.util.ArrayList"}
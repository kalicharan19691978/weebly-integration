package com.square;

import java.io.Serializable;

public class WeeblyData implements Serializable {
	private static final long serialVersionUID = 1L;
	String Creation_Date, Transaction_Date, Business_Event, Product_Line, Trx_currency_code, 
		Service_Start_Date, Service_End_Date, Trx_country_code, Settlement_country_code, Settlement_currency_code,
		Settlement_Date, Payment_Processor, Settled_Record, Cash_Flow, UUID, File_Type;
	String Trx_Line_Subtotal, Trx_Line_Tax_Amount, Trx_Line_total, Trx_Order_Total, 
		Est_Sett_Trx_Line_Subtotal, Est_Sett_Trx_Line_Tax_Amount, Est_Sett_Trx_Line_total,
		Est_Sett_Trx_Order_Total, Line_Subtotal_Settlement_Currency, Tax_Amount_Settlement_Currency,
		Line_total_Settlement_Currency, Order_Total_Settlement_Currency; 
	String Term_Length, Records_Count, Pipeline_run_id;
	

	
	
	
	public String getCreation_Date() {
		return Creation_Date;
	}
	public void setCreation_Date(String creation_Date) {
		Creation_Date = creation_Date;
	}
	public String getTransaction_Date() {
		return Transaction_Date;
	}
	public void setTransaction_Date(String transaction_Date) {
		Transaction_Date = transaction_Date;
	}
	public String getBusiness_Event() {
		return Business_Event;
	}
	public void setBusiness_Event(String business_Event) {
		Business_Event = business_Event;
	}
	public String getProduct_Line() {
		return Product_Line;
	}
	public void setProduct_Line(String product_Line) {
		Product_Line = product_Line;
	}
	public String getTrx_currency_code() {
		return Trx_currency_code;
	}
	public void setTrx_currency_code(String trx_currency_code) {
		Trx_currency_code = trx_currency_code;
	}
	public String getService_Start_Date() {
		return Service_Start_Date;
	}
	public void setService_Start_Date(String service_Start_Date) {
		Service_Start_Date = service_Start_Date;
	}
	public String getService_End_Date() {
		return Service_End_Date;
	}
	public void setService_End_Date(String service_End_Date) {
		Service_End_Date = service_End_Date;
	}
	public String getTrx_country_code() {
		return Trx_country_code;
	}
	public void setTrx_country_code(String trx_country_code) {
		Trx_country_code = trx_country_code;
	}
	public String getSettlement_country_code() {
		return Settlement_country_code;
	}
	public void setSettlement_country_code(String settlement_country_code) {
		Settlement_country_code = settlement_country_code;
	}
	public String getSettlement_currency_code() {
		return Settlement_currency_code;
	}
	public void setSettlement_currency_code(String settlement_currency_code) {
		Settlement_currency_code = settlement_currency_code;
	}
	public String getSettlement_Date() {
		return Settlement_Date;
	}
	public void setSettlement_Date(String settlement_Date) {
		Settlement_Date = settlement_Date;
	}
	public String getPayment_Processor() {
		return Payment_Processor;
	}
	public void setPayment_Processor(String payment_Processor) {
		Payment_Processor = payment_Processor;
	}
	public String getSettled_Record() {
		return Settled_Record;
	}
	public void setSettled_Record(String settled_Record) {
		Settled_Record = settled_Record;
	}
	public String getCash_Flow() {
		return Cash_Flow;
	}
	public void setCash_Flow(String cash_Flow) {
		Cash_Flow = cash_Flow;
	}
	public String getUUID() {
		return UUID;
	}
	public void setUUID(String uUID) {
		UUID = uUID;
	}
	public String getFile_Type() {
		return File_Type;
	}
	public void setFile_Type(String file_Type) {
		File_Type = file_Type;
	}
	public String getTrx_Line_Subtotal() {
		return Trx_Line_Subtotal;
	}
	public void setTrx_Line_Subtotal(String trx_Line_Subtotal) {
		Trx_Line_Subtotal = trx_Line_Subtotal;
	}
	public String getTrx_Line_Tax_Amount() {
		return Trx_Line_Tax_Amount;
	}
	public void setTrx_Line_Tax_Amount(String trx_Line_Tax_Amount) {
		Trx_Line_Tax_Amount = trx_Line_Tax_Amount;
	}
	public String getTrx_Line_total() {
		return Trx_Line_total;
	}
	public void setTrx_Line_total(String trx_Line_total) {
		Trx_Line_total = trx_Line_total;
	}
	public String getTrx_Order_Total() {
		return Trx_Order_Total;
	}
	public void setTrx_Order_Total(String trx_Order_Total) {
		Trx_Order_Total = trx_Order_Total;
	}
	public String getEst_Sett_Trx_Line_Subtotal() {
		return Est_Sett_Trx_Line_Subtotal;
	}
	public void setEst_Sett_Trx_Line_Subtotal(String est_Sett_Trx_Line_Subtotal) {
		Est_Sett_Trx_Line_Subtotal = est_Sett_Trx_Line_Subtotal;
	}
	public String getEst_Sett_Trx_Line_Tax_Amount() {
		return Est_Sett_Trx_Line_Tax_Amount;
	}
	public void setEst_Sett_Trx_Line_Tax_Amount(String est_Sett_Trx_Line_Tax_Amount) {
		Est_Sett_Trx_Line_Tax_Amount = est_Sett_Trx_Line_Tax_Amount;
	}
	public String getEst_Sett_Trx_Line_total() {
		return Est_Sett_Trx_Line_total;
	}
	public void setEst_Sett_Trx_Line_total(String est_Sett_Trx_Line_total) {
		Est_Sett_Trx_Line_total = est_Sett_Trx_Line_total;
	}
	public String getEst_Sett_Trx_Order_Total() {
		return Est_Sett_Trx_Order_Total;
	}
	public void setEst_Sett_Trx_Order_Total(String est_Sett_Trx_Order_Total) {
		Est_Sett_Trx_Order_Total = est_Sett_Trx_Order_Total;
	}
	public String getLine_Subtotal_Settlement_Currency() {
		return Line_Subtotal_Settlement_Currency;
	}
	public void setLine_Subtotal_Settlement_Currency(String line_Subtotal_Settlement_Currency) {
		Line_Subtotal_Settlement_Currency = line_Subtotal_Settlement_Currency;
	}
	public String getTax_Amount_Settlement_Currency() {
		return Tax_Amount_Settlement_Currency;
	}
	public void setTax_Amount_Settlement_Currency(String tax_Amount_Settlement_Currency) {
		Tax_Amount_Settlement_Currency = tax_Amount_Settlement_Currency;
	}
	public String getLine_total_Settlement_Currency() {
		return Line_total_Settlement_Currency;
	}
	public void setLine_total_Settlement_Currency(String line_total_Settlement_Currency) {
		Line_total_Settlement_Currency = line_total_Settlement_Currency;
	}
	public String getOrder_Total_Settlement_Currency() {
		return Order_Total_Settlement_Currency;
	}
	public void setOrder_Total_Settlement_Currency(String order_Total_Settlement_Currency) {
		Order_Total_Settlement_Currency = order_Total_Settlement_Currency;
	}
	public String getTerm_Length() {
		return Term_Length;
	}
	public void setTerm_Length(String term_Length) {
		Term_Length = term_Length;
	}
	public String getRecords_Count() {
		return Records_Count;
	}
	public void setRecords_Count(String records_Count) {
		Records_Count = records_Count;
	}
	public String getPipeline_run_id() {
		return Pipeline_run_id;
	}
	public void setPipeline_run_id(String pipeline_run_id) {
		Pipeline_run_id = pipeline_run_id;
	}
	
	
	
}

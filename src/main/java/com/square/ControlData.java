package com.square;

import java.io.Serializable;

public class ControlData implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String pipeline_run_id, total_amount, total_record_count;
	
	public String getPipeline_run_id() {
		return pipeline_run_id;
	}
	
	public void setPipeline_run_id(String pipeline_run_id) {
		this.pipeline_run_id = pipeline_run_id;
	}

	public String getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(String total_amount) {
		this.total_amount = total_amount;
	}

	public String getTotal_record_count() {
		return total_record_count;
	}

	public void setTotal_record_count(String total_record_count) {
		this.total_record_count = total_record_count;
	}
	
	
}

package com.square;

import java.util.ArrayList;

import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.TableResult;

public class ControlObj {
	
	public static ArrayList<ControlData> generateBean(TableResult payload) {
		ArrayList<ControlData> data = new ArrayList<ControlData>();
		for (FieldValueList row : payload.iterateAll()) {
			ControlData cd = new ControlData();
			
			if(!row.get("pipeline_run_id").isNull())
				cd.setPipeline_run_id(row.get("pipeline_run_id").getStringValue());
			if(!row.get("total_amount").isNull())
				cd.setTotal_amount(row.get("total_amount").getStringValue());
			if(!row.get("total_record_count").isNull())
				cd.setTotal_record_count(row.get("total_record_count").getStringValue());
			
			data.add(cd);
		}
		
		return data;
	}

}

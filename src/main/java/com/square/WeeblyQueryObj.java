package com.square;

import java.util.ArrayList;

import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.TableResult;

public class WeeblyQueryObj {
	
	public static ArrayList<WeeblyData> generateBean(TableResult payload) {
		ArrayList<WeeblyData> data = new ArrayList<WeeblyData>();
		
		for (FieldValueList row : payload.iterateAll()) {
			WeeblyData wd = new WeeblyData();
			if(!row.get("creation_date").isNull())
				wd.setCreation_Date(row.get("creation_date").getStringValue());
			if(!row.get("transaction_date").isNull())
				wd.setTransaction_Date(row.get("transaction_date").getStringValue());
			if(!row.get("business_event").isNull())
				wd.setBusiness_Event(row.get("business_event").getStringValue());
			if(!row.get("product_line").isNull())
				wd.setProduct_Line(row.get("product_line").getStringValue());
			if(!row.get("trx_currency_code").isNull())
				wd.setTrx_currency_code(row.get("trx_currency_code").getStringValue());
			if(!row.get("trx_line_subtotal").isNull())
				wd.setTrx_Line_Subtotal(row.get("trx_line_subtotal").getStringValue());
			if(!row.get("trx_line_tax_amount").isNull())
				wd.setTrx_Line_Tax_Amount(row.get("trx_line_tax_amount").getStringValue());
			if(!row.get("trx_line_total").isNull())
				wd.setTrx_Line_total(row.get("trx_line_total").getStringValue());
			if(!row.get("trx_order_total").isNull())
				wd.setTrx_Order_Total(row.get("trx_order_total").getStringValue());
			if(!row.get("est_sett_trx_line_subtotal").isNull())
				wd.setEst_Sett_Trx_Line_Subtotal(row.get("est_sett_trx_line_subtotal").getStringValue());
			if(!row.get("est_sett_trx_line_tax_amount").isNull())
				wd.setEst_Sett_Trx_Line_Tax_Amount(row.get("est_sett_trx_line_tax_amount").getStringValue());
			if(!row.get("est_sett_trx_line_total").isNull())
				wd.setEst_Sett_Trx_Line_total(row.get("est_sett_trx_line_total").getStringValue());
			if(!row.get("est_sett_trx_order_total").isNull())
				wd.setEst_Sett_Trx_Order_Total(row.get("est_sett_trx_order_total").getStringValue());
			if(!row.get("service_start_date").isNull())
				wd.setService_Start_Date(row.get("service_start_date").getStringValue());
			if(!row.get("term_length").isNull())
				wd.setTerm_Length(row.get("term_length").getStringValue());
			if(!row.get("service_end_date").isNull())
				wd.setService_End_Date(row.get("service_end_date").getStringValue());
			if(!row.get("trx_country_code").isNull())
				wd.setTrx_country_code(row.get("trx_country_code").getStringValue());
			if(!row.get("settlement_country_code").isNull())
				wd.setSettlement_country_code(row.get("settlement_country_code").getStringValue());
			if(!row.get("settlement_currency_code").isNull())
				wd.setSettlement_currency_code(row.get("settlement_currency_code").getStringValue());
			if(!row.get("settlement_date").isNull())
				wd.setSettlement_Date(row.get("settlement_date").getStringValue());
			if(!row.get("payment_processor").isNull())
				wd.setPayment_Processor(row.get("payment_processor").getStringValue());
			if(!row.get("line_subtotal_settlement_currency").isNull())
				wd.setLine_Subtotal_Settlement_Currency(row.get("line_subtotal_settlement_currency").getStringValue());
			if(!row.get("tax_amount_settlement_currency").isNull())
				wd.setTax_Amount_Settlement_Currency(row.get("tax_amount_settlement_currency").getStringValue());
			if(!row.get("line_total_settlement_currency").isNull())
				wd.setLine_total_Settlement_Currency(row.get("line_total_settlement_currency").getStringValue());
			if(!row.get("order_total_settlement_currency").isNull())
				wd.setOrder_Total_Settlement_Currency(row.get("order_total_settlement_currency").getStringValue());
			if(!row.get("settled_record").isNull())
				wd.setSettled_Record(row.get("settled_record").getStringValue());
			if(!row.get("cash_flow").isNull())
				wd.setCash_Flow(row.get("cash_flow").getStringValue());
			if(!row.get("uuid").isNull())
				wd.setUUID(row.get("uuid").getStringValue());
			if(!row.get("records_count").isNull())
				wd.setRecords_Count(row.get("records_count").getStringValue());
			if(!row.get("file_type").isNull())
				wd.setFile_Type(row.get("file_type").getStringValue());	
			if(!row.get("pipeline_run_id").isNull())
				wd.setPipeline_run_id(row.get("pipeline_run_id").getStringValue());
//			if(!row.get("system_id").isNull())
//				wd.setGeneration(row.get("system_id").getStringValue());
			data.add(wd);
		}
		return data;
	}

}

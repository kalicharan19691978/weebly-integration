package com.square;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class UtilityFunctions {

	public static boolean validateDate(String dt, boolean required) {
		if (dt == null || dt.isEmpty())
			return !required;
		else
			try {
				String pattern = "yyyy-MM-dd";
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
				Date date = simpleDateFormat.parse(dt);
				return date != null;
			} catch (Exception e) {
				return false;
			}
	}

	public static boolean validateNumber8(String num, boolean required) {
		if (num == null || num.isEmpty())
			return !required;
		else
			return Pattern.matches("^-?[0-9]*(\\.[0-9]{1,10})?$", num) && num.length() > 0;
	}

	public static boolean validateNumber8(String num, String fileType, boolean required) {

		if (fileType == "TRANSACTION") {
			return true;
		} else {
			if (num == null || num.isEmpty())
				return !required;
			else
				return Pattern.matches("^-?[0-9]*(\\.[0-9]{1,10})?$", num) && num.length() > 0;
		}
	}

	public static boolean validateWholeNumber(String num, boolean required) {
		if (num == null || num.isEmpty())
			return !required;
		else
			return Pattern.matches("[0-9]+", num);
	}
}
